public abstract class GameState {
    public static GameState EstadoNormal = new EstadoNormal();
    public static GameState JugadorPierde = new JugadorPierde();
    public static GameState JugadorGana = new JugadorGana();

    public abstract void writeResult();
}
