import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Board {

    public static final int ROW_OR_COLUMN_MINIMUM = 0;
    public static final int BOARD_DIMENSION = 4;
    private static final int MINES_AMOUNT = 1;
    private final Map<Coordinate, Cell> cells;
    private GameState gameState;

    Board() {
        cells = new HashMap<>();
        gameState = GameState.EstadoNormal;
    }

    public void initialize() {
        createEmptyCoordinates();
        addMinesToCells();
        addNumberOfMinesNearToCells();
    }

    private void addNumberOfMinesNearToCells() {
        cells.values().forEach(this::addNumberOfMinesNearToCell);
    }

    private void addNumberOfMinesNearToCell(final Cell cell) {
        if (cell.isMine()) {
            return;
        }
        int numberOfMinesNearToCell = 0;

        if (getCellTopLeft(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellTopCenter(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellTopRight(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellLeft(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellRight(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellBottomLeft(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellBottomCenter(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (getCellBottomRight(cell).isMine()) {
            numberOfMinesNearToCell++;
        }

        if (numberOfMinesNearToCell > 0) {
            final Cell newCell = new Cell(cell.getCoordinate(), CellType.Number);
            newCell.changeValue(numberOfMinesNearToCell);
            cells.put(cell.getCoordinate(), newCell);
        }

    }

    private Cell getCellTopCenter(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() - 1, originalCoordinate.getColumn());
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellTopRight(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() - 1, originalCoordinate.getColumn() + 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellTopLeft(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() - 1, originalCoordinate.getColumn() - 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellLeft(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow(), originalCoordinate.getColumn() - 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellRight(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow(), originalCoordinate.getColumn() + 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellBottomLeft(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() + 1, originalCoordinate.getColumn() - 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellBottomCenter(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() + 1, originalCoordinate.getColumn());
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private Cell getCellBottomRight(final Cell originalCell) {
        final Coordinate originalCoordinate = originalCell.getCoordinate();
        final Coordinate newCoordinate =
                new Coordinate(originalCoordinate.getRow() + 1, originalCoordinate.getColumn() + 1);
        if (isInvalidCoordinate(newCoordinate)) {
            return Cell.Incorrect;
        }
        return cells.get(newCoordinate);
    }

    private boolean isInvalidCoordinate(final Coordinate coordinate) {
        return !coordinate.valid();
    }

    private void addMinesToCells() {
        for (int mineCounter = 0; mineCounter < MINES_AMOUNT; mineCounter++) {
            Coordinate randomCoordinate;
            do {
                randomCoordinate = getRandomCoordinate();
            } while (!randomCoordinate.valid() && !canBeMine(randomCoordinate));
            cells.put(randomCoordinate, new Cell(randomCoordinate, CellType.Mine));
        }
    }

    private boolean canBeMine(final Coordinate coordinate) {
        return !cells.get(coordinate).isMine();
    }

    private Coordinate getRandomCoordinate() {
        return new Coordinate(new Random().nextInt(BOARD_DIMENSION), new Random().nextInt(BOARD_DIMENSION));
    }

    private void createEmptyCoordinates() {
        for (int row = ROW_OR_COLUMN_MINIMUM; row < BOARD_DIMENSION; row++) {
            for (int column = ROW_OR_COLUMN_MINIMUM; column < BOARD_DIMENSION; column++) {
                final Coordinate coordinate = new Coordinate(row, column);
                cells.put(coordinate, new Cell(coordinate, CellType.Blank));
            }
        }
    }

    public GameState getGameState() {
        return gameState;
    }

    public void processNewCoordinate(final Coordinate newCoordinate) {
        if (isInvalidCoordinate(newCoordinate)) {
            return;
        }

        final Cell cell = cells.get(newCoordinate);
        if (cell.isVisible()) {
            return;
        }

        if (cell.isMine()) {
            showAllMines();
            gameState = GameState.JugadorPierde;
            return;
        }

        cell.setVisible(true);

        if (cell.getCellType() == CellType.Number) {
            return;
        }

        processNewCoordinate(getCellTopLeft(cell).getCoordinate());
        processNewCoordinate(getCellTopCenter(cell).getCoordinate());
        processNewCoordinate(getCellTopRight(cell).getCoordinate());

        processNewCoordinate(getCellBottomLeft(cell).getCoordinate());
        processNewCoordinate(getCellBottomCenter(cell).getCoordinate());
        processNewCoordinate(getCellBottomRight(cell).getCoordinate());

        processNewCoordinate(getCellLeft(cell).getCoordinate());
        processNewCoordinate(getCellRight(cell).getCoordinate());
    }

    private void showAllMines() {
        cells.values().stream().filter(Cell::isMine).forEach(cell -> cell.setVisible(true));
    }

    public void show() {
        for (int i = ROW_OR_COLUMN_MINIMUM; i < BOARD_DIMENSION; i++) {
            System.out.print("\t\t" + i);
        }
        System.out.println();
        System.out.println();
        for (int row = ROW_OR_COLUMN_MINIMUM; row < BOARD_DIMENSION; row++) {
            System.out.print(row);
            for (int column = ROW_OR_COLUMN_MINIMUM; column < BOARD_DIMENSION; column++) {

                System.out.print("\t\t");

                final Coordinate coordinate = new Coordinate(row, column);
                if (cells.get(coordinate).isVisible()) {
                    System.out.print(cells.get(coordinate).getCellType().getValue());
                    System.out.print(" ");
                } else {
                    System.out.print("-");
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
