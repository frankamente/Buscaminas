public class Cell {
    public static Cell Incorrect = new Cell(Coordinate.Incorrect, CellType.Incorrect);
    private final Coordinate coordinate;
    private final CellType cellType;
    private boolean visible;

    Cell(final Coordinate coordinate, final CellType cellType) {
        this.coordinate = coordinate;
        this.cellType = cellType;
    }

    public boolean isMine() {
        return cellType==CellType.Mine;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void changeValue(final int numberOfMinesNearToCell) {
        if (cellType == CellType.Number){
            cellType.setValue(numberOfMinesNearToCell+"");
        }
    }

    public void setVisible(final boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public CellType getCellType() {
        return cellType;
    }
}
