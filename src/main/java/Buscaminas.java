public class Buscaminas {

    private Buscaminas() {
    }

    public static void main(String[] args) {
        final Buscaminas buscaminas = new Buscaminas();
        buscaminas.play();
    }

    private void play() {

        final Board board = new Board();
        final Player player = new Player();
        board.initialize();
        do {

            board.show();
            board.processNewCoordinate(player.getNewCoordinate());
        } while (board.getGameState() == GameState.EstadoNormal);

        board.show();
        board.getGameState().writeResult();
    }
}
