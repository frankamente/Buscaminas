import java.util.Objects;

public class Coordinate {
    public static Coordinate Incorrect = new Coordinate(-1,-1);
    private final int row;
    private final int column;


    Coordinate(final int row, final int column){

        this.row = row;
        this.column = column;
    }

    public boolean valid() {
        return rowValid() && columnValid();
    }

    private boolean columnValid() {
        return column >= Board.ROW_OR_COLUMN_MINIMUM && column < Board.BOARD_DIMENSION;
    }

    private boolean rowValid() {
        return row >= Board.ROW_OR_COLUMN_MINIMUM && row < Board.BOARD_DIMENSION;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Coordinate that = (Coordinate) o;
        return row == that.row &&
                column == that.column;
    }

    @Override
    public int hashCode() {

        return Objects.hash(row, column);
    }
}
