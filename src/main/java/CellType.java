public enum CellType {
    Incorrect("I"), Mine("*"), Blank("0"), Number("");

    private String value;

    CellType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        if (this == Number) {
            this.value = value;
        }
    }

}